push:
	@sudo git add *
	@sudo git commit
	@sudo git push
pull:
	@sudo git pull
test:
	@mkdir ../test
	@cp -r * ../test
	@cp -r .git ../test
	@./../test/decompile.sh
	@echo --------------------
	@echo Listing SRC
	@echo --------------------
	@ls ../test/src
clean:
	@rm -r -f ../test
transfer:
	@rm -r -f ../pvp-mcp
	@mkdir ../pvp-mcp
	@sudo cp -r * ../pvp-mcp
	@sudo cp -r .git ../pvp-mcp
	@sudo chmod 777 ../pvp-mcp
	@sudo chmod 777 ../pvp-mcp/.git
	@sudo chmod -R 777 ../pvp-mcp/*
perms:
	@sudo chmod -R 751 *
	@sudo chmod -R 775 *.sh
